from imghdr import what
import random
import model
import torch
import loader
from torchaudio import transforms
from torchvision import transforms
from torchvision.utils import save_image
from torch.utils.data import DataLoader
from torch import nn
from torch import optim
from statistics import mean

SEANTHESHEEP = 1

EPOCHS = 20
batch_sz = 30
width = 480
height = 480

# pos_weight = torch.ones([batch_sz, 1])  # All weights are equal to 1
# neg_weight = torch.zeros([batch_sz, 1])  # All weights are equal to 0
criterion = nn.BCEWithLogitsLoss()
auto_crit = nn.MSELoss()


def meanabserr(what_you_got, what_you_wanted):
    o = torch.mean(torch.abs(what_you_got - what_you_wanted))
    return o



img_t = transforms.Resize((width, height))

rd = loader.raveDataset(root_dir='frames', img_transform=img_t, audio_transform=None)
rdl = DataLoader(rd, batch_size=batch_sz, shuffle=True, num_workers=4)

device = torch.device('cuda:0')
gen = model.GenModel([batch_sz, width, height]).to(device)
# disc = model.DiscModel([batch_sz, height, width]).to(device)

optG = optim.Adam(gen.parameters(), lr=0.02)
# optD = optim.Adam(disc.parameters(), lr=0.02)

print(len(rdl))

# d_agg_loss = []
g_agg_loss = []

for epoch in range(EPOCHS):
    d_temp = []
    g_temp = []
    for i, data in enumerate(rdl):
        perc = (i / len(rdl)) * 100
        # print(len(data['image']))
        if len(data['image']) == batch_sz:
            
            # if perc % 2.0 == 0:
            
            img = data['image'].to(device=device, dtype=torch.float)
            audio = data['audio'].to(device=device, dtype=torch.float)
            # print(audio.shape)

            # train generator
            gen.zero_grad()
            fake_img = gen(audio)
            # print(m)
            g_loss = auto_crit(fake_img.cpu(), img.cpu())
            # print(g_loss)
            g_temp.append(g_loss.detach().mean().item())
            g_loss.backward()
            optG.step()

            print(round(perc, 4), mean(g_temp))
            if i % 100 == 0:
                fake = transforms.ToPILImage()(fake_img[0].cpu())
                real = transforms.ToPILImage()(img[0].cpu())
                fake.save('samples/' + str(epoch) + '_' + str(i) + '_generated.png')
                real.save('samples/' + str(epoch) + '_' + str(i) + '_src.png')

                # save_image(fake, 'samples/' + str(epoch) + '_' + str(i) + '_generated.png')
                # save_image(real, 'samples/' + str(epoch) + '_' + str(i) + '_src.png')
    # d_agg_loss.append(mean(d_temp))
    g_agg_loss.append(mean(g_temp))
    print("epoch", epoch, "done")
    # torch.save(disc, 'discriminator_' + str(epoch) + '.pth')
    torch.save(gen, 'generator_' + str(epoch) + '.pth')



