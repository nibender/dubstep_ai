import random
from time import sleep
from pytube import YouTube
import os
import csv
import cv2
import torch
import torchaudio
import math
import pandas as pd

video_location = 'videos'
frame_location = 'frames'
audio_location = 'audio'
dataset = 'dataset.csv'

# reset ripping:        rm -r frames; mkdir frames; rm dataset.csv; touch dataset.csv

# # read the csv file of youtube links
# with open('youtube_links.csv', 'r') as f:
#     reader = csv.reader(f)
#     links = list(reader)
#     for l in links:
#         print(l)
#         yt = YouTube(l[0])
#         # mp4_file = yt.streams.filter(file_extension='mp4', res='1080p').first()
#         a = yt.streams.filter(only_audio=True, file_extension='mp4', abr='128kbps').first()
#         # a = yt.streams.filter(only_audio=True, abr="160kbps").first()
#         print(a)
#         name = a.download(output_path=audio_location)
#         print(name)
#         short_name = name.replace(' ', '')
#         os.rename(name, short_name)
#         new_name = name.split('.')[0] + '.mp3'

#         print("\n\nmessing with audio\n\n")
        
#         os.system('ffmpeg -i "' + short_name + '" -ab 128k -ac 2 -ar 44100 -vn "' + new_name + '"')
#         os.system('rm "' + short_name + '"')

#         print("\n\ndownloading video\n\n")
        
#         v = yt.streams.filter(only_video=True, res='720p', progressive=False).first()
#         # print(v)
#         name = v.download(output_path=video_location)
#         new_name = name.replace(' ', '')
#         os.rename(name, new_name)

#         print('waiting to make youtube happy')

#         sleep(300)

# break down the mp4 files into frames
all_v = os.listdir(video_location)
print(all_v)
all_a = os.listdir(audio_location)
print(all_a)


for i in range(len(all_v)):
    sub_name = ''.join(random.sample(all_v[i], k=6))
    sub_dir_for_frames = frame_location + '/' + sub_name
    os.mkdir(sub_dir_for_frames)
    os.system('ffmpeg -i ' + video_location + '/"' + all_v[i] + '"  ' + sub_dir_for_frames + '/"' + all_v[i][:-4] + '"_%d.jpg')
    # count the number of frames
    all_frames = os.listdir(sub_dir_for_frames)
    all_frames = [f for f in all_frames if all_v[i][:-4] in f[:-4]]
    print(len(all_frames))

    # extract the audio from the mp4
    waveform, sample_rate = torchaudio.load(audio_location + '/' + all_a[i])
    waveform_samples = torch.chunk(waveform, len(all_frames), dim=1)
    print("sample chunks", len(waveform_samples))
    print("number of frames", len(all_frames))
    all_frames = all_frames[:-1]
    # if len(waveform_samples) > len(all_frames):
    #     print('error too many samples')

    #     break

    cnt = 0
    for j in range(len(waveform_samples)):
        
        t = torch.flatten(waveform_samples[j]).numpy().tolist()[:55*55]
        if len(t) == 55*55:
            t.insert(0, sub_name + '/' + all_frames[j])
            
            with open(dataset, 'a') as f:
                if cnt % 100:
                    print('writing to file good')
                writer = csv.writer(f)
                writer.writerow(t)
                cnt += 1
    print('wrote', cnt, 'lines to csv\n\n\n')

    print("processed:", all_v[i][:-4])

