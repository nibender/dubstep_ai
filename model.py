import torch
from torch import nn
from torch.nn import functional as F
import torchvision.transforms as T
from torch.nn import TransformerEncoder, TransformerEncoderLayer
import math

# Temporarily leave PositionalEncoding module here. Will be moved somewhere else.
class PositionalEncoding(nn.Module):
    r"""Inject some information about the relative or absolute position of the tokens in the sequence.
        The positional encodings have the same dimension as the embeddings, so that the two can be summed.
        Here, we use sine and cosine functions of different frequencies.
    .. math:
        \text{PosEncoder}(pos, 2i) = sin(pos/10000^(2i/d_model))
        \text{PosEncoder}(pos, 2i+1) = cos(pos/10000^(2i/d_model))
        \text{where pos is the word position and i is the embed idx)
    Args:
        d_model: the embed dim (required).
        dropout: the dropout value (default=0.1).
        max_len: the max. length of the incoming sequence (default=5000).
    Examples:
        >>> pos_encoder = PositionalEncoding(d_model)
    """

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        r"""Inputs of forward function
        Args:
            x: the sequence fed to the positional encoder model (required).
        Shape:
            x: [sequence length, batch size, embed dim]
            output: [sequence length, batch size, embed dim]
        Examples:
            >>> output = pos_encoder(x)
        """

        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class GenModel(nn.Module):
    def __init__(self, out_size, upscale_factor=9):
        super(GenModel, self).__init__()
        self.out_size = out_size
        encoder_layers = TransformerEncoderLayer(d_model=55*55, nhead=5, dim_feedforward=4096, dropout=0.1, batch_first=True)
        self.encoder = nn.TransformerEncoder(encoder_layer=encoder_layers, num_layers=15)

        # idk, learn something 
        # self.lin0 = nn.Linear(55*55, 6000)
        # self.lin1 = nn.Linear(6000, 6000)
        # self.lin2 = nn.Linear(6000, 6000)
        # self.lin3 = nn.Linear(6000, 55*55)
        # upscale
        self.l0 = nn.Conv2d(1, 64, (5, 5), (1, 1), (2, 2))
        self.l1 = nn.Conv2d(64, 64, (3, 3), (1, 1), (1, 1))
        self.l2 = nn.Conv2d(64, 32, (3, 3), (1, 1), (1, 1))
        self.l3 = nn.Conv2d(32, 3 * upscale_factor ** 2, (3, 3), (1, 1), (1, 1))
        self.pixel_shuffle = nn.PixelShuffle(upscale_factor)

        



    def forward(self, x):
        btc = x.shape[0]
        x = self.encoder(x, None)
        # print(x.shape)
        # x = F.leaky_relu(self.lin0(x))
        # x = F.leaky_relu(self.lin1(x))
        # x = F.leaky_relu(self.lin2(x))
        # x = F.leaky_relu(self.lin3(x))
        x = torch.reshape(x, (btc, 1, 55, 55))
        # print(x.shape)
        x = F.leaky_relu(self.l0(x))
        x = F.leaky_relu(self.l1(x))
        x = (torch.tanh(self.l2(x)) + 1) * 128
        x = self.l3(x)
        x = self.pixel_shuffle(x)
        
        # print(x.shape)
        # print("before interpolate:", x.shape)
        # cut the 
        # x = x.reshape(x.shape[0], 3, self.out_size[1], self.out_size[2])
        x = F.interpolate(x, size=(self.out_size[1], self.out_size[2]), mode='bilinear')
        # print("after interpolate:", x.shape)
        # mean, std = x.mean(), x.std()
        # x = 255 * (x - mean) / std
        return x



class SeparableConv2d(nn.Module):
    def __init__(self,in_channels,out_channels,kernel_size=1,stride=1,padding=0,dilation=1,bias=False):
        super(SeparableConv2d,self).__init__()

        self.conv1 = nn.Conv2d(in_channels,in_channels,kernel_size,stride,padding,dilation,groups=in_channels,bias=bias)
        self.pointwise = nn.Conv2d(in_channels,out_channels,1,1,0,1,1,bias=bias)

    def forward(self,x):
        x = self.conv1(x)
        x = self.pointwise(x)
        return x


class Block(nn.Module):
    def __init__(self,in_filters,out_filters,reps,strides=1,start_with_relu=True,grow_first=True):
        super(Block, self).__init__()

        if out_filters != in_filters or strides!=1:
            self.skip = nn.Conv2d(in_filters,out_filters,1,stride=strides, bias=False)
            self.skipbn = nn.BatchNorm2d(out_filters)
        else:
            self.skip=None

        rep=[]

        filters=in_filters
        if grow_first:
            rep.append(nn.ReLU(inplace=True))
            rep.append(SeparableConv2d(in_filters,out_filters,3,stride=1,padding=1,bias=False))
            rep.append(nn.BatchNorm2d(out_filters))
            filters = out_filters

        for i in range(reps-1):
            rep.append(nn.ReLU(inplace=True))
            rep.append(SeparableConv2d(filters,filters,3,stride=1,padding=1,bias=False))
            rep.append(nn.BatchNorm2d(filters))

        if not grow_first:
            rep.append(nn.ReLU(inplace=True))
            rep.append(SeparableConv2d(in_filters,out_filters,3,stride=1,padding=1,bias=False))
            rep.append(nn.BatchNorm2d(out_filters))

        if not start_with_relu:
            rep = rep[1:]
        else:
            rep[0] = nn.ReLU(inplace=False)

        if strides != 1:
            rep.append(nn.MaxPool2d(3,strides,1))
        self.rep = nn.Sequential(*rep)

    def forward(self,inp):
        x = self.rep(inp)

        if self.skip is not None:
            skip = self.skip(inp)
            skip = self.skipbn(skip)
        else:
            skip = inp

        x+=skip
        return x

class DiscModel_old(nn.Module):
    def __init__(self, in_size):
        super(DiscModel, self).__init__()
        self.in_size = in_size

        self.conv1 = nn.Conv2d(3, 32, 3, 2, 0, bias=False)
        self.bn1 = nn.BatchNorm2d(32)
        self.relu1 = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(32,64,3,bias=False)
        self.bn2 = nn.BatchNorm2d(64)
        self.relu2 = nn.ReLU(inplace=True)
        #do relu here

        self.block1=Block(64,128,2,2,start_with_relu=False,grow_first=True)
        self.block2=Block(128,256,2,2,start_with_relu=True,grow_first=True)
        self.block3=Block(256,728,2,2,start_with_relu=True,grow_first=True)

        self.block4=Block(728,728,3,1,start_with_relu=True,grow_first=True)
        self.block5=Block(728,728,3,1,start_with_relu=True,grow_first=True)
        self.block6=Block(728,728,3,1,start_with_relu=True,grow_first=True)
        self.block7=Block(728,728,3,1,start_with_relu=True,grow_first=True)

        self.block8=Block(728,728,3,1,start_with_relu=True,grow_first=True)
        self.block9=Block(728,728,3,1,start_with_relu=True,grow_first=True)
        self.block10=Block(728,728,3,1,start_with_relu=True,grow_first=True)
        self.block11=Block(728,728,3,1,start_with_relu=True,grow_first=True)

        self.block12=Block(728,1024,2,2,start_with_relu=True,grow_first=False)

        self.conv3 = SeparableConv2d(1024,1536,3,1,1)
        self.bn3 = nn.BatchNorm2d(1536)
        self.relu3 = nn.ReLU(inplace=True)

        #do relu here
        self.conv4 = SeparableConv2d(1536,2048,3,1,1)
        self.bn4 = nn.BatchNorm2d(2048)
        self.l4 = nn.Linear(in_features=460800, out_features=100)
        self.l5 = nn.Linear(in_features=100, out_features=1)
    
    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)

        x = self.conv2(x)
        x = self.bn2(x)
        x = self.relu2(x)

        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)
        x = self.block5(x)
        x = self.block6(x)
        x = self.block7(x)
        x = self.block8(x)
        x = self.block9(x)
        x = self.block10(x)
        x = self.block11(x)
        x = self.block12(x)

        x = self.conv3(x)
        x = self.bn3(x)
        x = self.relu3(x)

        x = self.conv4(x)
        x = self.bn4(x)
        x = x.view(x.size(0), -1)
        x = F.relu(self.l4(x))
        x = torch.tanh(self.l5(x))
        return x.cpu()



class DiscModel(nn.Module):
    def __init__(self, in_size):
        super(DiscModel, self).__init__()
        self.in_size = in_size

        self.conv1 = nn.Conv2d(3, 16, 3, 2, 0, bias=False)
        self.bn1 = nn.BatchNorm2d(16)
        self.relu1 = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(16,8,3,bias=False)
        self.bn2 = nn.BatchNorm2d(8)
        self.relu2 = nn.ReLU(inplace=True)
        
        self.l4 = nn.Linear(in_features=449352, out_features=100)
        self.l5 = nn.Linear(in_features=100, out_features=1)
    
    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)

        x = self.conv2(x)
        x = self.bn2(x)
        x = self.relu2(x)
        # print(x.shape)
        x = x.view(x.size(0), -1)
        x = F.relu(self.l4(x))
        x = torch.tanh(self.l5(x))
        return x.cpu()