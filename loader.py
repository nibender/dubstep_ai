from email.mime import audio
import torch 
from torch import nn
from torch.nn import functional as F
import torchaudio
import numpy as np
import os
import torchvision
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
import csv
import cv2
import pandas as pd
from io import StringIO

# https://pytorch.org/tutorials/beginner/data_loading_tutorial.html

class raveDataset(Dataset):
    def __init__(self, root_dir, img_transform=None, audio_transform=None):
        # self.rave_data = pd.read_csv('dataset.csv')
        self.rave_data = pd.read_pickle('rave_data.pkl')
        self.root_dir = root_dir
        self.img_t = img_transform
        self.audio_t = audio_transform

    def __len__(self):
        return len(self.rave_data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        img_name = os.path.join(self.root_dir, self.rave_data.iloc[idx, 0])
        img = torchvision.io.read_image(img_name, )
        # img = torch.from_numpy(img).transpose(0, 2)
        if self.img_t:
            img = self.img_t(img)
        audio = self.rave_data.iloc[idx, 1:].to_numpy()
        audio = (audio + 1) * 127
        # make the audio a square matrix
        ns = np.ceil(np.sqrt(audio.size)).astype(int)
        s = np.zeros(ns**2)
        s[:audio.size] = audio
        audio = torch.from_numpy(s.astype(np.double))[None, :]
        if self.audio_t:
            audio = self.audio_t(audio)
        sample = {'image': img, 'audio': audio}

        return sample
