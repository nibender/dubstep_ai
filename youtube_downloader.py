from time import sleep
from pytube import YouTube
import os
import csv
import cv2
import torch
import torchaudio
import math
import pandas as pd
import random

video_location = 'videos'
frame_location = 'frames'
audio_location = 'audio'
dataset = 'dataset.csv'

# read the csv file of youtube links
with open('youtube_links.csv', 'r') as f:
    reader = csv.reader(f)
    links = list(reader)
    for l in links:
        print(l)
        yt = YouTube(l[0])
        # mp4_file = yt.streams.filter(file_extension='mp4', res='1080p').first()
        a = yt.streams.filter(only_audio=True, file_extension='mp4', abr='128kbps').first()
        # a = yt.streams.filter(only_audio=True, abr="160kbps").first()
        print(a)
        name = a.download(output_path=audio_location)
        print(name)
        short_name = name.replace(' ', '')
        os.rename(name, short_name)
        new_name = name.split('.')[0] + '.mp3'

        print("\n\nmessing with audio\n\n")
        
        # os.system('ffmpeg -i "' + short_name + '" -ab 128k -ac 2 -ar 44100 -vn "' + new_name + '"')
        # os.system('rm "' + short_name + '"')

        print("\n\ndownloading video\n\n")
        
        # v = yt.streams.filter(only_video=True, res='720p', progressive=False).first()
        # print(v)
        # name = v.download(output_path=video_location, max_retries=10, timeout=5)
        # new_name = name.replace(' ', '')
        # os.rename(name, new_name)

        # print('waiting to make youtube happy')

        # sleep(300 + random.randrange(0, 300, 10))
